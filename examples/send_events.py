#!/usr/bin/env python3

from anodot_api.event import make_event
from anodot_api.session import AnodotAPISession
import csv
import dateutil.parser as dp
import dateutil
import pathlib


DRY_RUN = True  # if True, then only prints events

FILE_PATH = 'holidays_nsw.csv'
ENV_OR_ROOT = 'poc'
TOKEN = 'abcd1234de098765edbcabcbcd'


TIMEZONE = 'UTC+0'
CATEGORY = 'other'
SOURCE = 'other'
TITLE_COLUMN = 'Event name'
PROPERTY_COLUMNS = ['CARD_TYPE', 'TRANSPORT_MODE', 'Event Category']
START_TIME_COLUMN = 'Start Time'
END_TIME_COLUMN = 'End Time'  # May be None

timezone = dateutil.tz.tzstr(TIMEZONE)


def rec_to_event(rec):
    end_dttm = None
    if END_TIME_COLUMN is not None:
        try:
            end_dttm = dp.parse(rec[END_TIME_COLUMN])
            if end_dttm:
                end_dttm = end_dttm.replace(tzinfo=timezone)
        except Exception:
            pass

    return make_event(
                start_dttm=dp.parse(rec[START_TIME_COLUMN]).replace(tzinfo=timezone),
                end_dttm=end_dttm,
                category=CATEGORY,
                source=SOURCE,
                title=rec[TITLE_COLUMN],
                property_pairs=[(key, rec[key]) for key in PROPERTY_COLUMNS])


def main():
    input_file = pathlib.Path(FILE_PATH)
    events = []
    for rec in csv.DictReader(input_file.open('rt', encoding='utf-8-sig')):
        evt = rec_to_event(rec)
        print(evt)
        events.append(evt)

    if not DRY_RUN:
        session = AnodotAPISession(env_or_root=ENV_OR_ROOT, token=TOKEN)
        session.post_events(events)


if __name__ == '__main__':
    main()
