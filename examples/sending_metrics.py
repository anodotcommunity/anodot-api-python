#!/usr/bin/env python
'''sending_metrics.py - example for sending a single metric with value 1 for the current time
the metric properties ( for looking up inside Anodot ) are:
ver=tests
test=sending_metrics
what=always1
'''

from anodot_api.sample import make_sample
from anodot_api.session import AnodotAPISession

ANODOT_ENVIRONMENT = # should be 'poc' or 'production'
ANODOT_TOKEN = # put a string with your token here

if __name__ == '__main__':
    session = AnodotAPISession( env_or_root = ANODOT_ENVIRONMENT, token = ANODOT_TOKEN)
    samples = [ make_sample(dttm = datetime.utcnow(), property_pairs = dict(ver = 'tests', test = 'sending_metrics', what = 'always1').items(), value = 1.0 ) ]
    session.post_samples(samples)

