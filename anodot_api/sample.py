from datetime import datetime
from collections import OrderedDict
from .timestamp import get_unix_timestamp
from .clean import clean_props

def make_sample( *, dttm = None, property_pairs = None, tag_pairs = None, value = None , should_clean = True, base_obj = OrderedDict() ):
    assert dttm is not None
    assert any( (( p[0] == 'what' for p in property_pairs )) )
    ret = OrderedDict(base_obj)
    ret['properties'] = OrderedDict( property_pairs if not should_clean else clean_props(property_pairs) )
    ret['timestamp'] = get_unix_timestamp(dttm)
    ret['value'] = value
    if tag_pairs is not None:
        ret['tags'] = { k: [v] for (k,v) in clean_props(tag_pairs) }

    return ret   

def shift_timestamp(sample, timestamp_shift):
    return OrderedDict(sample, timestamp = sample['timestamp'] + timestamp_shift)
