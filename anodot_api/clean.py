import re
import logging
LOG = logging.getLogger(__name__)

PAT_REPL_UNDERSCORE = re.compile('(/|:| |\.|\+|-|!|\^|&|\[|\]|\{|\}|<|>|~|\||"|\?|=|[^\x00-\x7F])')
PAT_REMOVE = re.compile(r"[\'\(\)]")
PAT_COMPACT = re.compile('_+')
    
def clean_string(s):
    """clean_string
    Cleans `s` by removing characters that may interfere with Anodot search.

    :param s: str - the string to clean.
    """
    try:
        if s == '':
            return 'EMPTY'
        ret = s
        if type(s) == bytes:
            ret = s.decode('ascii', errors = 'ignore')
        elif type(s) != str:
            ret = str(s)
        
        ret = PAT_REMOVE.sub('', ret)
        ret = PAT_REPL_UNDERSCORE.sub('_', ret)
        ret = PAT_COMPACT.sub('_', ret)
        ret = ret.strip('_')
        if ret == '': 
            return 'INVALID'
        return ret
    except Exception as e:
        LOG.error("problem cleaning `{}`".format(s), exc_info = 1)
        raise e

def add_cmp_key(prop):
    """ convert the prop = (key, val) to lexicographic ordering 
        with what, target_type last
    """
    return ({ 'collector': -1, 'what': 1, 'target_type': 2 }.get(prop[0],0),prop)

def clean_props(property_pairs):
    return [(   clean_string( prop_name )[:50],
                clean_string( prop_val )[:150])
                        for (prop_name, prop_val) in property_pairs ]

