#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import islice
from time import sleep
import json
import logging
import requests
import os
from urllib.parse import urljoin

LOG = logging.getLogger(__name__)

ANODOT_ENV_MAP = {
        'production': 'https://api.anodot.com',
        'poc': 'https://api-poc.anodot.com'
}

DEFAULT_API_CHUNKSIZE = 1000
DEFAULT_API_DELAY = 1

class AnodotPartialError(Exception):
    pass

class AnodotAPISession(object):
    """AnodotAPISession"""
    def __init__(self, *,
            token = None,
            env_or_root = None,
            api_delay = DEFAULT_API_DELAY, 
            api_chunksize = DEFAULT_API_CHUNKSIZE ):
        """__init__

        :param token:
        :param env_or_root: either `poc` or `production` or a full url of the Anodot API root
        :param api_delay: delay in seconds between consecutive requests
        :param api_chunksize: maximum number of entries (samples or events) to send in a single request
        """
        
        self.session = requests.Session()
        self.token = token or os.environ.get('ANODOT_API_TOKEN')
        self.api_delay = api_delay
        self.api_chunksize = api_chunksize
        self.anodot_root = ANODOT_ENV_MAP.get(env_or_root,env_or_root) or os.environ.get('ANODOT_API_ENDPOINT') or ANODOT_ENV_MAP.get('production')

        self._verified_event_sources = {}
        self._verified_event_categories = {}

    @property
    def metrics_endpoint(self):
        return urljoin(self.anodot_root, "api/v1/metrics")

    @property
    def events_endpoint(self):
        return urljoin(self.anodot_root, "api/v1/user-events")

    @property
    def events_categories_endpoint(self):
        return urljoin(self.anodot_root, "api/v1/user-events/categories")

    @property
    def events_sources_endpoint(self):
        return urljoin(self.anodot_root, "api/v1/user-events/sources")


    def _post_samples_chunk(self, content):
        headers = { 'Content-Type':'application/json' }
        LOG.debug("Sending {} samples in chunk: {} ...".format(len(content), json.dumps(content[:3], indent = 2)))
        
        resp = self.session.post(
                self.metrics_endpoint,
                params = dict(
                    token = self.token,
                    protocol = "anodot20"),
                headers = headers,
                json = content )

        resp.raise_for_status()

        response_data = resp.json()
        partial_errors = response_data.get('errors')
        if partial_errors:
            raise AnodotPartialError(partial_errors)

    def post_samples(self, samples, *, clean = True):
        chunk_size = self.api_chunksize
        for i in range((len(samples) + chunk_size - 1) // chunk_size):
            LOG.debug("sleeping {} seconds before next chunk".format(self.api_delay))
            if i > 0:
                sleep(self.api_delay)
            chunk = [ sample for sample in
                        islice(samples,i * chunk_size,(i+1)*chunk_size) ]
            LOG.debug("sending chunk of {} samples".format(len(chunk)))
            self._post_samples_chunk( chunk )

#    def _verify_event_sources(self, event_sources, auto_add = True):
#        for source in event_sources:
#            self.session.get(self.event_sources_endpoint, source)

    def post_events(self, events):
        for e in events:
            resp = self.session.post(
                    self.events_endpoint,
                    params = dict(token = self.token),
                    json = e
                    )

            LOG.debug("got " + str(resp.content))

            
