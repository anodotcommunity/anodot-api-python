from .timestamp import get_unix_timestamp
from .clean import clean_props, clean_string

def make_event( *, start_dttm, end_dttm = None, category, source, title, description = None, property_pairs = None):
    return { 
            "event": {
                "category": category,
                "source": source,
                "title": clean_string(title),
                "description": clean_string(description),
                "startDate": get_unix_timestamp(start_dttm),
                "endDate": get_unix_timestamp(end_dttm) if end_dttm else None,
                "properties": [ { "key": k, "value": v } for (k,v) in clean_props(property_pairs) ]
                }
            }

