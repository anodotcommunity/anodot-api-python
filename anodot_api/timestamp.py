from datetime import datetime
from dateutil.tz import gettz, tzutc

UTC = tzutc()
EPOCH = datetime(1970, 1, 1, tzinfo = UTC)

def get_unix_timestamp(dt: datetime):
    """Returns a unix_timestamp. If `dt` is a naive timestamp (has no tzinfo), we assume it has tz = UTC."""
    if dt.tzinfo is None:
        dt = dt.replace(tzinfo = UTC)
    return int(( dt - EPOCH ).total_seconds())

