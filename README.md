anodot-api-python - Anodot REST API implementation in Python
------------------------------------------------------------

Anodot is an anomaly detection platform.

This package implements the customer-facing APIs used for sending metrics and events.
We use requests for HTTP/S requests and marshmallow for data validation.

We intend to support Python (3+) but not *legacy* Python (2.7-)
