#!/usr/bin/python
from setuptools import setup, find_packages
import importlib
import os
import sys
import re


def find_version():
    version_file_path = os.path.join(os.path.abspath(os.path.dirname(__file__)),'anodot_api','version.py')
    globals_ = {}
    locals_ = {}
    exec(compile(open(version_file_path,'rb').read(), version_file_path, 'exec'), globals_, locals_)
    return locals_['FULL_VERSION']

setup(name='anodot-api-python',
    version=find_version(),
    description='Anodot REST API library for Python 3.6+',
    author='Moran Cohen',
    author_email='moran.cohen@anodot.com',
    url='https://www.anodot.com',
    packages = find_packages(),
    python_requires = '>=3.6',
    scripts=[],
    install_requires = [
        'requests'
    ]
)

